# Let's create a shell script file containing the provided aliases.

aliases = """
# Azure CLI Aliases

# Resource Groups
alias azrg="az group"
alias azrgc="az group create"
alias azrgl="az group list"
alias azrgs="az group show"
alias azrgd="az group delete"

# Virtual Machines
alias azvm="az vm"
alias azvmc="az vm create"
alias azvml="az vm list"
alias azvms="az vm show"
alias azvmss="az vm start"
alias azvmstop="az vm stop"
alias azvmdealloc="az vm deallocate"
alias azvmdelete="az vm delete"

# Virtual Networks
alias azvnet="az network vnet"
alias azvnetc="az network vnet create"
alias azvnetl="az network vnet list"
alias azvnets="az network vnet show"
alias azvnetd="az network vnet delete"

# Subnets
alias azsubnet="az network vnet subnet"
alias azsubnetc="az network vnet subnet create"
alias azsubnetl="az network vnet subnet list"
alias azsubnets="az network vnet subnet show"
alias azsubnetd="az network vnet subnet delete"

# Storage Accounts
alias azstorage="az storage account"
alias azstoragec="az storage account create"
alias azstoragel="az storage account list"
alias azstorages="az storage account show"
alias azstoraged="az storage account delete"

# Key Vaults
alias azkv="az keyvault"
alias azkvc="az keyvault create"
alias azkvl="az keyvault list"
alias azkvs="az keyvault show"
alias azkvd="az keyvault delete"

# Azure Active Directory
alias azad="az ad"
alias azaduser="az ad user"
alias azaduserc="az ad user create"
alias azaduserl="az ad user list"
alias azadusers="az ad user show"
alias azaduserd="az ad user delete"

# Container Services
alias azacs="az aks"
alias azacsc="az aks create"
alias azacsl="az aks list"
alias azacss="az aks show"
alias azacsd="az aks delete"

# Functions
alias azfunc="az functionapp"
alias azfuncc="az functionapp create"
alias azfuncl="az functionapp list"
alias azfuncs="az functionapp show"
alias azfuncd="az functionapp delete"

# SQL Databases
alias azsql="az sql db"
alias azsqlc="az sql db create"
alias azsqll="az sql db list"
alias azsqls="az sql db show"
alias azsqld="az sql db delete"
"""

# Save the aliases to a file
file_path = '/mnt/data/azure_cli_aliases.sh'
with open(file_path, 'w') as file:
    file.write(aliases)

file_path
