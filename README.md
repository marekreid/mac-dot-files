### All of my dot file config

NB: Package installation is handled by custom go installer that uses the package-list.txt in this repo - obv just the chmod for the first time


```
chmod +x package-installer && ./package-installer package-list.txt
```

## Once the packages have been setup the terminal can be configured by installing oh-my-zsh

```
sh -c "$(wget https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
```

## Then lazy-vim can be installed

```
git clone https://github.com/LazyVim/starter ~/.config/nvim 
```

## Wallpaper is also in this directory as bg.jpg

## Now most everything should be setup - just run 

```
stow .
```


