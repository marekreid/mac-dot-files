local wezterm = require("wezterm")

return {
	color_schemes = {
		["Catppuccin Macchiato Custom"] = {
			foreground = "#FFD1DC", -- Pastel pink
			background = "#24273A",
			cursor_bg = "#F4DBD6",
			cursor_border = "#F4DBD6",
			cursor_fg = "#24273A",
			selection_bg = "#363A4F",
			selection_fg = "#CAD3F5",
			ansi = { "#494D64", "#ED8796", "#A6DA95", "#EED49F", "#8AADF4", "#F5BDE6", "#8BD5CA", "#B8C0E0" },
			brights = { "#5B6078", "#ED8796", "#A6DA95", "#EED49F", "#8AADF4", "#F5BDE6", "#8BD5CA", "#A5ADCB" },
			tab_bar = {
				background = "#1E2030",
				active_tab = {
					bg_color = "#363A4F",
					fg_color = "#CAD3F5",
				},
				inactive_tab = {
					bg_color = "#1E2030",
					fg_color = "#CAD3F5",
				},
				inactive_tab_hover = {
					bg_color = "#363A4F",
					fg_color = "#CAD3F5",
				},
				new_tab = {
					bg_color = "#1E2030",
					fg_color = "#CAD3F5",
				},
				new_tab_hover = {
					bg_color = "#24273A",
					fg_color = "#CAD3F5",
				},
			},
		},
	},
	color_scheme = "Catppuccin Macchiato Custom",
	enable_tab_bar = false,
	font_size = 16.0,
	font = wezterm.font("IosevkaTerm Nerd Font"),
	macos_window_background_blur = 30,
	window_background_opacity = 0.78,
	window_decorations = "RESIZE",
	keys = {
		{
			key = "f",
			mods = "CTRL",
			action = wezterm.action.ToggleFullScreen,
		},
	},
	mouse_bindings = {
		{
			event = { Up = { streak = 1, button = "Left" } },
			mods = "CTRL",
			action = wezterm.action.OpenLinkAtMouseCursor,
		},
	},
}
